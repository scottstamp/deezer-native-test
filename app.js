const ipc = require('node-ipc');
const DeezerNative = require('./node-deezer-native/index.js');

function callFunction(functionName, params) {
    if (this.isNativeProcessLoaded !== true && ['stop', 'shutDownPlayer', 'shutDownConnect'].indexOf(functionName) !== -1) {
        return;
    }
    const callId = uuid();
    const defer = new Defer();
    try{
        ipc.of.nodeDeezerNative.emit('execFunction', {callId, functionName, params});
    } catch(e) {
        log.info(`Could not call method ${functionName} on native player`);
    }
    this.callDefers[callId] = defer;
    return defer.promise;
}

async function init() {
var resp = await this.callFunction('initConnect', [{
    disableLog: false,
    appId: 'DEEZER_NODE_ELECTRON',
    productId: null,
    productBuildId: null,
    userProfilePath: this.getCachePath(),
    accessToken: 'NULL'
}]);

console.log(resp);
}

init();