# Node.js Deezer Native SDK Addon

This module encapsulate the Deezer Native SDK and exposes methods to control the SDK.

This is still largely Work In Progress

Compilation has been tested on OS X El Capitan

Linux will compile but segfault on init

Windows has not been tested yet

## Initialization

```javascript
// Init the connect handler
DeezerNative.initConnect({
appId: '00000', // Your Deezer Developer App ID
productId: 'XXXX', // Your product ID
productBuildId: 'XXXX', // Your product build ID
accessToken: 'YOUR_TOKEN', // Your user token
userProfilePath: '/your/path', // Path where data is stored
// (defaults to 'c:\\\\dzr\\\\dzrcache_NDK_SAMPLE' on windows, '/var/tmp/dzrcache_NDK_SAMPLE' on Unix systems)
disableLog: false, // Disable log (defaults to true)
});
// Init the player handler
DeezerNative.initPlayer();
```

## Listen for events

```javascript
DeezerNative.onPlayerEvent((event) => console.log('Play', event));

DeezerNative.onConnectCacheEvent((event) => console.log('ConnectCache', event));

DeezerNative.onConnectEvent((event) => console.log('Connect', event));

DeezerNative.onOfflineEvent((event) => console.log('Offline', event));

DeezerNative.onEvent((event) => console.log('Event', event));

DeezerNative.onPlayerReady(() => {
console.log('Player Ready, you can start playing a track :)');
});
```

TODO : List events

## Play a track

```javascript
// Load a dzmedia URL (cf Native SDK docs)
DeezerNative.loadUrl(\"dzmedia:///track/3135554\");
DeezerNative.play();
```

## Misc controls

```javascript
// Set internal volume of the player
// On linux, will change pulseaudio sink volume
DeezerNative.setVolume(100);
```

## Offline Synchronization

```javascript
// Synchronize some content
DeezerNative.syncAddContent(
// Path of the resource in the db (can be pretty much anything)
'/dzlocal/tracklist/album/302127',
// Checksum to check for current sync state
'XXXX',
// Array of track Ids
[
3135553,
3135554,
3135555,
3135556,
3135557,
3135558,
3135559,
3135560,
3135561,
3135562,
3135563,
3135564,
3135565,
3135566,
]
);

// Remove some content
DeezerNative.syncRemoveContent(
'/dzlocal/dzlocal/tracklist/album/302127'
);
```

## Testing

To run tests, you need to provide app and token via env

You can also specify which media is played by the integration test

`DZ_TOKEN=XXXXX DZ_APP_ID=XXXX DZ_MEDIA_URL=\"dzmedia:///track/TRACKID\" npm run test`

## Next steps

- Better event handling (adding event data) - DONE
- Add reporting functions for offline sync
- More misc functions (crossfade, shuffle ...)
- Exception handling with code transformation to message (error types already exposed in DeezerNative.Errors, but not sent through exceptions)