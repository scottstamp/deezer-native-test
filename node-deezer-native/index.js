const EventEmitter = require('events');
const DeezerNative = require(__dirname + '/build/Release/DeezerNative.node');

function invert(data) {
	return Object.keys(data).reduce((obj, key) => {
		obj[data[key]] = key;
		return obj;
	}, {});
}

DeezerNative.Event = {
	Connect: Object.assign({}, DeezerNative.Constants.ConnectEvents),
	ConnectCache: Object.assign({}, DeezerNative.Constants.ConnectCacheEvents),
	Player: Object.assign({}, DeezerNative.Constants.PlayerEvents),
	Metadata: Object.assign({}, DeezerNative.Constants.MetadataEvents),
	Offline: Object.assign({}, DeezerNative.Constants.OfflineEvents),
	Categories: Object.assign({}, DeezerNative.Constants.Categories)
};
Object.freeze(DeezerNative.Event);
Object.freeze(DeezerNative.Event.Connect);
Object.freeze(DeezerNative.Event.ConnectCache);
Object.freeze(DeezerNative.Event.Player);
Object.freeze(DeezerNative.Event.Metadata);
Object.freeze(DeezerNative.Event.Offline);
Object.freeze(DeezerNative.Event.Categories);

DeezerNative.Error = Object.assign({}, DeezerNative.Constants.ErrorCodes);
Object.freeze(DeezerNative.Error);

const idToError = invert(DeezerNative.Error);
Object.freeze(idToError);

const playerIdToEventType = invert(DeezerNative.Event.Player);
Object.freeze(playerIdToEventType);

const metadataIdToEventType = invert(DeezerNative.Event.Metadata);
Object.freeze(metadataIdToEventType);

const connectIdToEventType = invert(DeezerNative.Event.Connect);
Object.freeze(connectIdToEventType);

const connectCacheIdToEventType = invert(DeezerNative.Event.ConnectCache);
Object.freeze(connectCacheIdToEventType);

const offlineIdToEventType = invert(DeezerNative.Event.Offline);
Object.freeze(offlineIdToEventType);

const categoryIdToType = invert(DeezerNative.Event.Categories);
Object.freeze(categoryIdToType);

let allEventTypes = {};
allEventTypes[DeezerNative.Event.Categories.Player] = playerIdToEventType;
allEventTypes[DeezerNative.Event.Categories.Metadata] = metadataIdToEventType;
allEventTypes[DeezerNative.Event.Categories.Connect] = connectIdToEventType;
allEventTypes[DeezerNative.Event.Categories.ConnectCache] = connectCacheIdToEventType;
allEventTypes[DeezerNative.Event.Categories.Offline] = offlineIdToEventType;
Object.freeze(allEventTypes);
Object.freeze(allEventTypes[DeezerNative.Event.Categories.Player]);
Object.freeze(allEventTypes[DeezerNative.Event.Categories.Metadata]);
Object.freeze(allEventTypes[DeezerNative.Event.Categories.Connect]);
Object.freeze(allEventTypes[DeezerNative.Event.Categories.ConnectCache]);
Object.freeze(allEventTypes[DeezerNative.Event.Categories.Offline]);

class DeezerEventEmitter extends EventEmitter {}
const deezerEventEmitter = new DeezerEventEmitter();

let eventInterval = false;

function startEventInterval() {
	if (eventInterval === false) {
		eventInterval = setInterval(function() {
			let lastEvents = DeezerNative.getLastEvents();
			if (typeof lastEvents === 'object') {
				lastEvents.forEach((event) => {
					event = Object.assign({}, event);
					let eventCategory = categoryIdToType[event.eventCategoryId];
					let eventType = allEventTypes[event.eventCategoryId][event.eventTypeId];
					const eventName = 'Dz.Event.' + eventCategory + '.' + eventType;
					let data = {};
					try {
						data = JSON.parse(event.data);
					} catch (e) {
						data = {
							failedJson: event.data
						};
					}
					const eventData = Object.assign({}, event, {eventCategory, eventType, eventName, data});
					deezerEventEmitter.emit('Dz.Event', eventData);
					deezerEventEmitter.emit('Dz.Event.' + eventCategory, eventData);
					deezerEventEmitter.emit(eventName, eventData);
				});
			}
		}, 40);
	}
}

function stopEventInterval() {
	if (eventInterval !== false && DeezerNative.isConnectInitialized() === false && DeezerNative.isPlayerInitialized() === false) {
		clearInterval(eventInterval);
	}
	eventInterval = false;
}

DeezerNative.onPlayerEvent = (cb) => {
	deezerEventEmitter.on('Dz.Event.Player', (data) => {
		cb(data);
	});
};
DeezerNative.onConnectEvent = (cb) => {
	deezerEventEmitter.on('Dz.Event.Connect', (data) => {
		cb(data);
	});
};
DeezerNative.onConnectCacheEvent = function(cb) {
	deezerEventEmitter.on('Dz.Event.ConnectCache', (data) => {
		cb(data);
	});
};
DeezerNative.onOfflineEvent = function(cb) {
	deezerEventEmitter.on('Dz.Event.Offline', (data) => {
		cb(data);
	});
};
DeezerNative.onEvent = function(cb) {
	deezerEventEmitter.on('Dz.Event', (data) => {
		cb(data);
	});
};

DeezerNative.onPlayerReady = () => {
	if (DeezerNative.isPlayerInitialized()) {
		return Promise.resolve();
	}
	return new Promise((resolve, reject) => {
		deezerEventEmitter.once('Dz.Event.Connect.UserLoginOk', resolve);
	});
};

DeezerNative.initConnect = (config) => {
	startEventInterval();
	DeezerNative._initConnect(Object.assign({}, config));
};

DeezerNative.initPlayer = (config) => {
	config = config || {};
	startEventInterval();
	DeezerNative._initPlayer(Object.assign({
		reportLoadingProgress: true,
		reportPlaybackProgress: true
	}, config));
};

DeezerNative.shutDownConnect = () => {
	returnValue = DeezerNative._shutDownConnect();
	stopEventInterval();
	return returnValue;
};

DeezerNative.shutDownPlayer = () => {
	returnValue = DeezerNative._shutDownPlayer();
	stopEventInterval();
	return returnValue;
};

/**
 * Added content to synchronize
 *
 * @param path string
 * @param checksum string
 * @param tracks int[]
 */
DeezerNative.syncAddContent = (path, checksum, tracks) => {
	let prop = 'tracks';
	let dataObject = {};
	if (tracks.length && isNaN(parseInt(tracks[0], 10))) {
		prop = 'medias';
	}
	dataObject[prop] = tracks.map((trackId) => !isNaN(parseInt(trackId, 10)) && isFinite(trackId)
		? parseInt(trackId, 10)
		: trackId);
	const jsonData = JSON.stringify(dataObject);
	DeezerNative._syncAddContent(path, checksum, jsonData);
};

// TODO Impl interval cancel on player exit
module.exports = DeezerNative;
