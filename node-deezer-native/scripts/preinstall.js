#!/usr/bin/env node

const process = require('process');
const path = require('path');
const fs = require('fs');

if (process.platform === 'darwin') {
	console.log('Current platform is ' + process.platform + ', creating framework symlinks');

	const currentDir = path.join(__dirname, '..');
	const frameworkDir = path.join(currentDir, 'deps/DeezerNativeSDK/Bins/Platforms/mac/x64/libdeezer.framework');
	const versionsDir = path.join(currentDir, 'deps/DeezerNativeSDK/Bins/Platforms/mac/x64/libdeezer.framework/Versions');

	const cwd = process.cwd();

	process.chdir(versionsDir);
	const versionLinkTarget = 'A';
	const versionLinkPath = 'Current';
	if (!fs.existsSync(versionLinkPath)) {
		fs.symlinkSync(versionLinkTarget, versionLinkPath);
	}

	process.chdir(frameworkDir);
	const execLinkTarget = 'Versions/Current/libdeezer';
	const execLinkPath = 'libdeezer';
	if (!fs.existsSync(execLinkPath)) {
		fs.symlinkSync(execLinkTarget, execLinkPath);
	}

	const headersLinkTarget = 'Versions/Current/Headers';
	const headersLinkPath = 'Headers';
	if (!fs.existsSync(headersLinkPath)) {
		fs.symlinkSync(headersLinkTarget, headersLinkPath);
	}

	const ResourcesLinkTarget = 'Versions/Current/Resources';
	const ResourcesLinkPath = 'Resources';
	if (!fs.existsSync(ResourcesLinkPath)) {
		fs.symlinkSync(ResourcesLinkTarget, ResourcesLinkPath);
	}

	process.chdir(cwd);
}
