#!/usr/bin/env node

const path = require('path');
const exec = require('child_process').exec;

if (process.platform === 'darwin') {

	console.log('Current platform is ' + process.platform + ', special linking is needed for libdeezer');

	const originPath = '@executable_path/../Frameworks/libdeezer.framework/Versions/A/libdeezer';
	const currentDir = path.join(__dirname, '..');
	const execFolder = path.join(currentDir, 'build/Release');
	const newPath = path.relative(execFolder, path.join(currentDir, 'deps/DeezerNativeSDK/Bins/Platforms/mac/x64/libdeezer.framework/Versions/A/libdeezer'));
	const execPath = path.join(execFolder, 'DeezerNative.node');
	const command = `install_name_tool -change ${originPath} @loader_path/${newPath} ${execPath}`;

	console.log('Running ' + command);
	exec(command, function(error, stdout, stderr) {
		console.log('libdeezer Linked');
	});

}
